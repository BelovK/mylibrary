using UnityEngine;

namespace Lib
{
    public class PoolObject : MonoBehaviour
    {
        private Pool _parentPool;

        public virtual void Init(Pool pool)
        {
            _parentPool = pool;
        }

        public virtual void Activate()
        {
            gameObject.SetActive(true);
        } 

        public virtual void Deactivate()
        {
            _parentPool.ConfigureDeactivated(this);
            gameObject.SetActive(false);
        }

    }
}
