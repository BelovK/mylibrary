using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Lib
{
    public class Pool : MonoBehaviour
    {
        private const int MAX_POOL_SIZE = 100;

        public event Action OnInitialized;
        public bool Initialized { get; private set; }

        [Tooltip("How many pool objects needs in same time in some moment")]
        [SerializeField] private int _initianalPoolSize = 20;

        /// <summary>
        ///The prefab instantiate
        /// </summary>
        [SerializeField] private PoolObject objectPrefab;

        private Queue<PoolObject> _objects;

        private int _realPoolSize;

        public GameObject GetObject(bool activateNow = true)
        {
            if (_objects == null || !Initialized)
            {
                Debug.LogWarning("[Pool] Pool not initialized");
                return null;
            }
            if (_objects.Count == 0)
            {
                if (_realPoolSize < MAX_POOL_SIZE)
                {
                    GenerateNewObjectInPool();
                }
                else
                {
                    throw new System.Exception("Pool stack overflow");
                }
            }
            PoolObject newObject = _objects.Dequeue();
            if (activateNow)
                newObject.Activate();
            return newObject.gameObject;
        }

        public void ConfigureDeactivated(PoolObject poolObject)
        {
            _objects.Enqueue(poolObject);
        }

        protected async void InitStartPool()
        {
            Initialized = false;
            _objects = new Queue<PoolObject>(_initianalPoolSize);
            for (int i = 0; i < _initianalPoolSize; i++)//Make start pool
            {
                GenerateNewObjectInPool();
                await Task.Yield();
            }
            Initialized = true;
            OnInitialized?.Invoke();
        }

        /// <summary>
        /// Instantiate poolobject, hide it, and add it on Queue
        /// </summary>
        /// <returns></returns>
        private PoolObject GenerateNewObjectInPool()
        {
            PoolObject newObject = Instantiate(objectPrefab, transform);
            newObject.gameObject.SetActive(false);
            newObject.Init(this);
            _objects.Enqueue(newObject);
            _realPoolSize++;
            return newObject;
        }

        private void Start()
        {
            InitStartPool();
        }

    }
}
