

#if UNITASK_ADDRESSABLE_SUPPORT
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Assertions;

public class AddressablesPool : MonoBehaviour
{
    public bool IsReady { get { return loadingCoroutine == null; } }

    [SerializeField] private int elementCount = 8;
    [SerializeField] private AssetReference assetReferenceToInstantiate = null;

    private static Dictionary<object, AddressablesPool> allAvailablePools = new Dictionary<object, AddressablesPool>();
    private Queue<GameObject> pool = null;
    private Coroutine loadingCoroutine;

    public static AddressablesPool GetPool(AssetReference assetReference)
    {
        var exists = allAvailablePools
            .TryGetValue(assetReference.RuntimeKey, out AddressablesPool pool);
        if (exists)
        {
            return pool;
        }

        return null;
    }

    public GameObject Take(Transform parent)
    {
        Assert.IsTrue(IsReady, $"Pool {name} is not ready yet");
        if (IsReady == false) return null;
        if (pool.Count > 0)
        {
            var newGameObject = pool.Dequeue();
            newGameObject.transform.SetParent(parent, false);
            newGameObject.SetActive(true);
            return newGameObject;
        }

        return null;
    }

    public void Return(GameObject gameObjectToReturn)
    {
        gameObjectToReturn.SetActive(false);
        gameObjectToReturn.transform.parent = transform;
        pool.Enqueue(gameObjectToReturn);
    }


    void OnEnable()
    {
        Assert.IsTrue(elementCount > 0, "Element count must be greater than 0");
        Assert.IsNotNull(assetReferenceToInstantiate, "Prefab to instantiate must be non-null");
        allAvailablePools[assetReferenceToInstantiate.RuntimeKey] = this;
        loadingCoroutine = StartCoroutine(SetupPool());
    }

    void OnDisable()
    {
        allAvailablePools.Remove(assetReferenceToInstantiate);
        foreach (var obj in pool)
        {
            Addressables.ReleaseInstance(obj);
        }
        pool = null;
    }

    private IEnumerator SetupPool()
    {
        pool = new Queue<GameObject>(elementCount);
        for (var i = 0; i < elementCount; i++)
        {
            var handle = assetReferenceToInstantiate.InstantiateAsync(transform);
            yield return handle;
            var newGameObject = handle.Result;
            pool.Enqueue(newGameObject);
            newGameObject.SetActive(false);
        }

        loadingCoroutine = null;
    }

}
#endif
