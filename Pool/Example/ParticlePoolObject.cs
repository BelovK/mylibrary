using UnityEngine;

namespace Lib
{
    public class ParticlePoolObject : PoolObject
    {
        public ParticleSystem Particle { get => _particle; }
        [SerializeField]
        private ParticleSystem _particle;

        private void OnEnable()
        {
            this.Invoke(() => Deactivate(), _particle.main.duration);
        }
    }
}
