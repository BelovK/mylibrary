using System.Collections.Generic;
using UnityEngine;

namespace Lib
{
    public abstract class ScriptableDataBase<T> : ScriptableObject
    {
        public int DataCount => _data.Count;

        [SerializeField] protected List<T> _data;

        protected abstract bool EqualsNames(T data, string name);

        public virtual void AddData(T data)
        {
            _data.Add(data);
        }

        public bool Contains(T data)
        {
            return _data.Contains(data);
        }

        public T GetData(int index)
        {
            if (index > DataCount || index < 0)
            {
                Debug.LogError("[DATA] GetData index OUT OF RANGE = " + index + ". Is return clamp data");
            }
            return _data[Mathf.Clamp(index, 0, DataCount)];
        }

        public T GetData(string name)
        {
            return _data.Find(x => EqualsNames(x, name));
        }

        public T GetRandomData()
        {
            return _data[Random.Range(0, _data.Count)];
        }
    }
}