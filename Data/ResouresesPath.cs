using UnityEngine;

namespace Lib
{
    public abstract class ResouresesPath : ScriptableObject
    {
        [field: SerializeField] public string AssetPath { get; private set; }

        public abstract GameObject PrefabGameObject { get; }
#if UNITY_EDITOR
        protected void FindAssetPath(GameObject asset)
        {
            AssetPath = Lib.AssetHelper.GetResourcesPath(asset);
        }

        protected void OnValidate()
        {
            FindAssetPath(PrefabGameObject);
        }
#endif
    }
}
