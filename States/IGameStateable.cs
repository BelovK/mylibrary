using System;

namespace Lib
{
    public interface IGameStateable
    {
        public event Action<GameStates> OnGameStateChanged;
        public GameStates GameStates { get; set; }

    }
}
