namespace Lib
{
    public enum GameStates
    {
        Init,
        Preparing,
        Playing,
        Paused,
        Finished,
        Exited
    }
}
