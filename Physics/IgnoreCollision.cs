using UnityEngine;

namespace Lib
{

    public class IgnoreCollision : MonoBehaviour
    {
        [SerializeField] private bool _ignoreOnAwake = true;
        [SerializeField] private Collider _thisCollider;
        [SerializeField] private Collider[] _othersColliders;

        public void SetIgnore(bool value)
        {
            foreach (Collider item in _othersColliders)
            {
                Physics.IgnoreCollision(_thisCollider, item, value);
            }
        }

        private void Awake()
        {
            if (_ignoreOnAwake)
            {
                SetIgnore(true);
            }
        }

    }
}
