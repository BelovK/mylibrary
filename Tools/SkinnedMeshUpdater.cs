﻿using UnityEngine;

namespace Lib
{
    public static class SkinnedMeshUpdater
    {
        public static string ResetBones(SkinnedMeshRenderer targetSkin, Transform rootBone, bool includeInactive = false)
        {
            string statusText = "== Processing bones... ==";
            // Look for root bone
            string rootName = "";
            if (targetSkin.rootBone != null) rootName = targetSkin.rootBone.name;
            Transform newRoot = null;
            // Reassign new bones
            Transform[] newBones = new Transform[targetSkin.bones.Length];
            Transform[] existingBones = rootBone.GetComponentsInChildren<Transform>(includeInactive);
            int missingBones = 0;
            for (int i = 0; i < targetSkin.bones.Length; i++)
            {
                if (targetSkin.bones[i] == null)
                {
                    statusText += System.Environment.NewLine + "WARN: Do not delete the old bones before the skinned mesh is processed!";
                    missingBones++;
                    continue;
                }
                string boneName = targetSkin.bones[i].name;
                bool found = false;
                foreach (var newBone in existingBones)
                {
                    if (newBone.name == rootName) newRoot = newBone;
                    if (newBone.name == boneName)
                    {
                        statusText += System.Environment.NewLine + "· " + newBone.name + " found!";
                        newBones[i] = newBone;
                        found = true;
                    }
                }
                if (!found)
                {
                    statusText += System.Environment.NewLine + "· " + boneName + " missing!";
                    missingBones++;
                }
            }
            targetSkin.bones = newBones;
            statusText += System.Environment.NewLine + "Done! Missing bones: " + missingBones;
            if (newRoot != null)
            {
                statusText += System.Environment.NewLine + "· Setting " + rootName + " as root bone.";
                targetSkin.rootBone = newRoot;
            }
            Debug.Log(statusText);
            return statusText;
        }
    }
}