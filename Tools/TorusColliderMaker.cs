using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Lib
{
#if UNITY_EDITOR
    [CustomEditor(typeof(TorusColliderMaker))]
    public class TorusColliderMakerEditor : Editor
    {
        private TorusColliderMaker Target { get { return target as TorusColliderMaker; } }
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if (GUILayout.Button("Update colliders"))
            {
                Target.CreateColliders();
            }
            if (GUILayout.Button("Destroy colliders"))
            {
                Target.DestoryColliders();
            }
        }
    }
#endif
    public class TorusColliderMaker : MonoBehaviour
    {
        public Vector3 Center { get => transform.position + _center; }

        [SerializeField] private bool _isTrigger;
        private bool _oldTigger;
        //TODO debug this
        //[SerializeField]
        private Vector3 _center;
        private Vector3 _oldCenter;
        [SerializeField] private Vector3 _boxSize;
        private Vector3 _oldBoxSize;
        [SerializeField] private float _radius;
        private float _oldRadius;
        [Range(3, 16)]
        [SerializeField] private int _boxCount;
        private int _oldCount;

        [HideInInspector]
        [SerializeField]
        private List<GameObject> _boxes;

#if UNITY_EDITOR
        private void OnValidate()
        {
            if (!_boxSize.Equals(_oldBoxSize))
            {
                UpdateSize();
                _oldBoxSize = _boxSize;
            }
            if (!_radius.Equals(_oldRadius))
            {
                UpdatePositions();
                _oldRadius = _radius;
            }
            if (!_center.Equals(_oldCenter))
            {
                UpdatePositions();
                _oldCenter = _center;
            }
            if (!_boxCount.Equals(_oldCount))
            {
                CreateColliders(true);
                _oldCount = _boxCount;
            }
            if (_isTrigger.Equals(_oldTigger))
            {
                UpdateTiggerState();
                _oldTigger = _isTrigger;
            }
        }
#endif
        public void CreateColliders(bool onValidateInvoke = false)
        {
            if (_boxes == null)
            {
                _boxes = new List<GameObject>(_boxCount);
            }
            if (_boxes.Count > 0 && _boxes.Count > _boxCount)
            {
                while (_boxes.Count != _boxCount)
                {
                    if (onValidateInvoke)
                    {
                        WaitAndDestoy(_boxes.RemoveLast());
                    }
                    else
                    {
                        DestroyImmediate(_boxes.RemoveLast());
                    }

                }
                UpdatePositions();
            }
            if (_boxes.Count == 0 || _boxes.Count < _boxCount)
            {
                while (_boxes.Count != _boxCount)
                {
                    _boxes.Add(GameObject.CreatePrimitive(PrimitiveType.Cube));
                    GameObject box = _boxes.Last();
                    Renderer renderer;
                    if (box.TryGetComponent<Renderer>(out renderer))
                    {
                        if (onValidateInvoke)
                        {
                            WaitAndDestoy(renderer);
                        }
                        else
                        {
                            DestroyImmediate(renderer);
                        }
                    }
                    box.transform.SetParent(transform);
                    box.transform.localPosition = Vector3.zero;
                    box.transform.localScale = _boxSize;
                }
                UpdatePositions();
            }
        }

        public void DestoryColliders()
        {
            if (_boxes != null)
            {
                if (_boxes.Count > 0)
                {
                    while (_boxes.Count != 0)
                    {
                        DestroyImmediate(_boxes.RemoveLast());
                    }
                }
                _boxes.Clear();
            }
            else
            {
                Debug.Log("Not found colliders for destroy");
            }
        }

        private async void WaitAndDestoy(Object obj)
        {
            await Task.Yield();
            DestroyImmediate(obj);
        }

        private void UpdateSize()
        {
            if (_boxes != null)
            {
                foreach (GameObject box in _boxes)
                {
                    box.transform.localScale = _boxSize;
                }
            }
        }

        private void UpdateTiggerState()
        {
            if (_boxes != null)
            {
                foreach (GameObject box in _boxes)
                {
                    box.GetComponent<Collider>().isTrigger = _isTrigger;
                }
            }
        }

        private void UpdatePositions()
        {
            if (_boxes != null)
            {
                for (int i = 0; i < _boxes.Count; i++)
                {
                    _boxes[i].transform.position = GetPosition(i);
                    _boxes[i].transform.rotation = Quaternion.LookRotation((_boxes[i].transform.position - Center).normalized, Vector3.forward);
                }
            }
        }

        private Vector3 GetPosition(int index)
        {
            Vector3 rotateCenter = transform.rotation * _center;
            float x = 0;
            float y = 0;
            x = rotateCenter.x + _radius * Mathf.Cos(2 * Mathf.PI * index / _boxCount);//x[i] = x0 + R * cos(2 * PI * i / n),
            y = rotateCenter.y + _radius * Mathf.Sin(2 * Mathf.PI * index / _boxCount);//y[i] = y0 + R * sin(2 * PI * i / n).
            return transform.position + transform.TransformDirection(new Vector3(x, y, rotateCenter.z));
        }

        //private void OnDrawGizmosSelected()
        //{
        //    Vector3 rotateCenter = transform.rotation * _center;
        //    for (int i = 0; i < _boxCount; i++)
        //    {
        //        Gizmos.DrawLine(transform.position + transform.TransformDirection(rotateCenter), GetPosition(i));
        //        //Gizmos.DrawWireCube(centerGlobal + transform.TransformDirection(new Vector3(x, y, rotateCenter.z)), _boxSize);
        //        //_boxes.Count
        //        //Debug.DrawLine(centerGlobal, centerGlobal + new Vector3(x, y, 0), Color.green, 5f);
        //    }
        //}


    }
}
