using UnityEngine;
using UnityEditor;

namespace Lib
{
#if UNITY_EDITOR
    public class UpdateSkinnedMeshWindow : EditorWindow
    {
        [MenuItem("Window/Update Skinned Mesh Bones")]
        public static void OpenWindow()
        {
            var window = GetWindow<UpdateSkinnedMeshWindow>();
            window.titleContent = new GUIContent("Skin Updater");
        }
        private GUIContent _statusContent = new GUIContent("Waiting...");
        private SkinnedMeshRenderer _targetSkin;
        private Transform _rootBone;
        private bool _includeInactive;
        private string _statusText = "Waiting...";

        private void OnGUI()
        {
            _targetSkin = EditorGUILayout.ObjectField("Target", _targetSkin, typeof(SkinnedMeshRenderer), true) as SkinnedMeshRenderer;
            _rootBone = EditorGUILayout.ObjectField("RootBone", _rootBone, typeof(Transform), true) as Transform;
            _includeInactive = EditorGUILayout.Toggle("Include Inactive", _includeInactive);
            bool enabled = (_targetSkin != null && _rootBone != null);
            if (!enabled)
            {
                _statusText = "Add a target SkinnedMeshRenderer and a root bone to process.";
            }
            GUI.enabled = enabled;
            if (GUILayout.Button("Update Skinned Mesh Renderer"))
            {
                _statusText = SkinnedMeshUpdater.ResetBones(_targetSkin, _rootBone);//MAIN
            }
            _statusContent.text = _statusText;
            EditorStyles.label.wordWrap = true;
            GUILayout.Label(_statusContent);
        }
    }
#endif
}