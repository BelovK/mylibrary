﻿using UnityEngine;

namespace Lib
{
	public class StaticBatchRoot : MonoBehaviour
	{
		[Header("Place this script on root object")]
		[SerializeField] private Renderer[] _childs;

		private GameObject[] _gameObjects;
		private void Awake()
		{
			_childs = transform.gameObject.GetComponentsInChildren<Renderer>();
			if (_childs.Length == 0)
			{
				Debug.LogError("StaticBatchRoot did't found renderer in childs this root", gameObject);
				return;
			}
			_gameObjects = new GameObject[_childs.Length];
			for (int i = 0; i < _childs.Length; i++)
			{
				_gameObjects[i] = _childs[i].gameObject;
			}
			StaticBatchingUtility.Combine(_gameObjects, gameObject);
		}
	}
}
