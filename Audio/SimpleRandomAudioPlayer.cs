using UnityEngine;

namespace Lib
{
    public class SimpleRandomAudioPlayer : MonoBehaviour
    {
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip[] _audioClips;
        [SerializeField] private Vector2 _pitchRange = new Vector2(0.8f, 1.3f);
        [Header("Loop options")]
        [SerializeField] private bool _repeatOnEnable = true;
        [SerializeField] private Vector2 _delayRange = new Vector2(5f, 20f);


        private Coroutine _waitRepeat;

        public void Play()
        {
            if (_audioClips.Length == 0)
            {
                Debug.LogError("[SimpleRandomAudioPlayer] audio clips is empty");
                this.enabled = false;
                return;
            }
            AudioClip clip = _audioClips[Random.Range(0, _audioClips.Length)];
            Play(_audioSource, clip);
        }
        public void Play(AudioSource audioSource, AudioClip clip)
        {
            audioSource.pitch = Random.Range(_pitchRange.x, _pitchRange.y);
            audioSource.PlayOneShot(clip);
        }

        public void Play(AudioClip clip)
        {
            Play(_audioSource, clip);
        }

        public void PlayAndRepeat()
        {
            Play();
            WaitAndPlayRepeat();
        }

        public void SetClips(AudioClip[] clips) => _audioClips = clips;

        private void WaitAndPlayRepeat()
        {
            _waitRepeat = this.Invoke(PlayAndRepeat, Random.Range(_delayRange.x, _delayRange.y));
        }

        private void OnEnable()
        {
            if (_repeatOnEnable)
                WaitAndPlayRepeat();
        }

        private void OnDisable()
        {
            if (_waitRepeat != null)
            {
                StopCoroutine(_waitRepeat);
            }
        }
    }
}
