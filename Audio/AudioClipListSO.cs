using System.Collections.Generic;
using UnityEngine;

namespace Lib
{
    [CreateAssetMenu(fileName = "AudioClipList", menuName = "Audio Clip List")]
    public class AudioClipListSO : ScriptableObject
    {
        [SerializeField] private List<AudioClip> _audioClips;

        public AudioClip[] GetAudioClips() => _audioClips.ToArray();

        public AudioClip GetAudio(int index) => _audioClips[index];
    }
}
