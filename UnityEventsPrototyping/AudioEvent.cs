﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Lib
{
	public class AudioEvent : MonoBehaviour
	{
		public UnityEvent OnTime, OnFinish;

		[SerializeField] private AudioSource _audioSource;
		[SerializeField] private Vector2 _timeRange;
		

		private bool inRange = false;
		public bool IsPlaying
		{
			set
			{
				bool _oldValue = _isPlaying;
				_isPlaying = value;
				if (!_oldValue.Equals(_isPlaying))
				{
					if (_isPlaying)
					{
						inRange = false;
						StartCoroutine(WaitForSound());
					}
				}

			}
			get
			{
				return _isPlaying;
			}
		}
		private bool _isPlaying = false;

		// Start is called before the first frame update
		void Start()
		{
			if (!_audioSource)
			{
				_audioSource = GetComponent<AudioSource>() ?? GetComponentInChildren<AudioSource>();
			}
			_isPlaying = false;
		}

		// Update is called once per frame
		void Update()
		{
			if (_audioSource.isPlaying && !IsPlaying)
			{
				IsPlaying = true;

			}
			if (_audioSource.time > _timeRange.x && _audioSource.time < _timeRange.y && !inRange)
			{
				inRange = true;
				OnTime.Invoke();
			}
			//if (audioSource.time >= audioSource.clip.length)
			//{
			//	onFinish.Invoke();
			//}
		}

		IEnumerator WaitForSound()
		{
			yield return new WaitForSeconds(_audioSource.clip.length);
			OnFinish?.Invoke();
		}
	}
}