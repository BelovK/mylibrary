using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Lib
{
	public class DelayListInvoke : MonoBehaviour
	{
		public List<EventDelayPair> EventDelayPairs;

		public void InvokeAll()
		{
			foreach (var eventDelayPair in EventDelayPairs)
			{
				eventDelayPair.unityEvent?.Invoke();
			}
		}

		private void Start()
		{
			foreach (var eventDelayPair in EventDelayPairs)
			{
				StartCoroutine(InvokeDelayed(eventDelayPair));
			}
		}

		private IEnumerator InvokeDelayed(EventDelayPair pair)
		{
			var timer = pair.Delay;

			do
			{
				timer -= Time.deltaTime;
				pair.Delay = timer;
				yield return null;
			} while (timer > 0);

			pair.Delay = 0;

			pair.unityEvent.Invoke();


		}
	}
	[Serializable]
	public class EventDelayPair
	{
		public UnityEvent unityEvent;
		public float Delay;
	}
}
