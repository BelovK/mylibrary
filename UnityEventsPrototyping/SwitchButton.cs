﻿using UnityEngine;
using UnityEngine.Events;
namespace Lib
{
	public class SwitchButton : MonoBehaviour
	{
		public bool isSwitch = false;

		public KeyCode keyCode;

		public UnityEvent onTurn, onReturn;


		public void Click()
		{
			if (isSwitch)
			{
				onReturn.Invoke();
			}
			else
			{
				onTurn.Invoke();
			}
			isSwitch = !isSwitch;
		}

		// Update is called once per frame
		void Update()
		{
			if (Input.GetKeyDown(keyCode))
			{
				Click();
			}
		}
	}
}
