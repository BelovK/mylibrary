﻿using UnityEngine;
using UnityEngine.Events;

namespace Lib
{

	/// <summary>
	/// Only for prototyping
	/// </summary>
	public class InvokerEvent : MonoBehaviour
	{
		[Multiline]
		public string tip;

		public UnityEvent onEvent;

		public bool onEnable = true;
		public float time = 0.2f;
		public bool randomTime = false;
		public Vector2 randomRange;

		private void OnEnable()
		{
			if (onEnable)
			{
				if (randomTime)
				{
					Invoke(nameof(InvokeEvent), Random.Range(randomRange.x, randomRange.y));
				}
				else
				{
					InvokeWithDelay();
				}
			}
		}

		public void InvokeEvent()
		{
			onEvent.Invoke();
		}

		public void InvokeWithDelay()
		{
			Invoke(nameof(InvokeEvent), time);
		}

	}
}
