﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Lib
{
	public class OnButtonDown : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
	{
		public const float PRESS_UPDATE_TIME = 0.1f;

		public UnityEvent OnDown, OnUp, OnPress, OnSelect, OnDeselect;

		private bool _isPressing = false;


		public void OnPointerEnter(PointerEventData eventData)
		{
			OnSelect?.Invoke();
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			OnDeselect?.Invoke();
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			OnDown?.Invoke();
			_isPressing = true;
			StartCoroutine(Press());
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			OnUp?.Invoke();
			_isPressing = false;
			StopAllCoroutines();
		}

		private IEnumerator Press()
		{
			while (_isPressing)
			{
				OnPress?.Invoke();
				yield return new WaitForSeconds(PRESS_UPDATE_TIME);
			}
		}

		private void OnEnable()
		{
			_isPressing = false;
		}


	}
}
