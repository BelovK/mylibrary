using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Lib
{
	public class UnityEventKeyInvoker : MonoBehaviour
	{
		public UnityEvent onDown;
		public KeyCode key;

		private void Update()
		{
			if (Input.GetKeyDown(key))
			{
				onDown?.Invoke();
			}
		}
	}
}
