﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Lib
{
	public class SelectableButton : Selectable
	{
		public const float PRESS_UPDATE_TIME = 0.1f;

		public UnityEvent onDown, onUp, onPress, onSelect;

		private bool _isPressing = false;


		public override void OnPointerEnter(PointerEventData eventData)
		{
			base.OnPointerEnter(eventData);
			onSelect?.Invoke();
		}

		public override void OnPointerDown(PointerEventData eventData)
		{
			base.OnPointerDown(eventData);
			onDown?.Invoke();
			_isPressing = true;
			StartCoroutine(Press());
		}

		public override void OnPointerUp(PointerEventData eventData)
		{
			base.OnPointerUp(eventData);
			onUp?.Invoke();
			_isPressing = false;
			StopAllCoroutines();
		}

		private IEnumerator Press()
		{
			while (_isPressing)
			{
				onPress?.Invoke();
				yield return new WaitForSeconds(PRESS_UPDATE_TIME);
			}
		}




	}
}
