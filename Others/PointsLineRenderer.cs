using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsLineRenderer : MonoBehaviour
{
    [SerializeField] private bool _fixedUpdate = true;
    [SerializeField] private bool _onlyOnEnable = false;

    [SerializeField] private List<Transform> _points = new List<Transform>();
    [SerializeField] private LineRenderer _lineRenderer;

    private void FixedUpdate()
    {
        if (_fixedUpdate)
        {
            UpdateLines();
        }
    }

    private void Update()
    {
        if (!_fixedUpdate)
        {
            UpdateLines();
        }
    }

    [ContextMenu("UpdatePoints")]
    private void UpdateLines()
    {
        for (int i = 0; i < _points.Count; i++)
        {
            _lineRenderer.SetPosition(i, _points[i].position);
        }
    }

    private void OnEnable()
    {
        if (_onlyOnEnable)
        {
            UpdateLines();
            this.enabled = false;
        }
    }
}
