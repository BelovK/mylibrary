using UnityEngine;

public class ParentController : MonoBehaviour
{

	[SerializeField] private bool _nullPositionOnStart = true;

	private Transform _startParent;
	private Vector3 _startLocalPosition;
	private Quaternion _startLocalRotation;

	public void SetParentNull()
	{
		transform.SetParent(null);
	}

	public void ResetParent(bool resetPosition)
	{
		transform.SetParent(_startParent);
		if (resetPosition)
		{
			transform.localPosition = _startLocalPosition;
			transform.localRotation = _startLocalRotation;
		}
	}

	private void Start()
	{
		_startParent = transform.parent;
		_startLocalPosition = _startParent.localPosition;
		_startLocalRotation = _startParent.localRotation;
		if (_nullPositionOnStart)
		{
			SetParentNull();
		}
	}
}
