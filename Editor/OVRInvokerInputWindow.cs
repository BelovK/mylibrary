using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR && OVR
namespace Lib
{
    public class OVRInvokerInputWindow : EditorWindow
	{

		protected KeyCode _keyCode;

		private GUIStyle _yellow;

		private Vector2 _scrollPos;
		private List<OVRUnityActionInvoker> _keyInvokersAll = new List<OVRUnityActionInvoker>();
		protected OVRUnityActionInvoker _keyInvoker;

		[MenuItem("Window/OVRInvokerInputWindow")]
		public static void ShowWindow()
		{
			GetWindow<OVRInvokerInputWindow>("OVR invoker input");
		}

		private void OnGUI()
		{
			_scrollPos = EditorGUILayout.BeginScrollView(_scrollPos, GUILayout.Width(position.width), GUILayout.Height(position.height - 50f));
			foreach (var item in _keyInvokersAll)
			{
				EditorGUILayout.BeginHorizontal();
				_keyInvoker = EditorGUILayout.ObjectField(item, typeof(OVRUnityActionInvoker), GUILayout.Width(100)) as OVRUnityActionInvoker;
				StringBuilder stringBuilder = new StringBuilder();
				if (item.onClick != null)
				{
					if (item.onClick.GetPersistentEventCount() > 0)
					{
						stringBuilder.Append(item.onClick.GetPersistentTarget(0).name);
						stringBuilder.Append(".");
						stringBuilder.Append(item.onClick.GetPersistentMethodName(0));
					}
					else
					{
						stringBuilder.Append("Empty");
					}
				}
				else
				{
					stringBuilder.Append("Error");
				}

				GUILayout.Label(stringBuilder.ToString(), GUILayout.Width(180f));

				item.button = (OVRInput.Button)EditorGUILayout.EnumPopup(item.button, GUILayout.Width(100f));
				item.controller = (OVRInput.Controller)EditorGUILayout.EnumPopup(item.controller, GUILayout.Width(50f));
				item.pressType = (PressType)EditorGUILayout.EnumPopup(item.pressType, GUILayout.Width(70f));
				OVRUnityActionInvoker unityEventKey = _keyInvokersAll.Find(x => x.button == item.button && x != item && x.controller == item.controller && x.pressType == item.pressType);
				if (unityEventKey)
				{
					GUILayout.Label("!!!", _yellow);
				}

				EditorGUILayout.EndHorizontal();
			}
			EditorGUILayout.EndScrollView();
		}

		private void OnInspectorUpdate()
		{
			_yellow = new GUIStyle(EditorStyles.label);
			_yellow.normal.textColor = Color.yellow;

			_keyInvokersAll.Clear();
			_keyInvokersAll.AddRange(FindObjectsOfType<OVRUnityActionInvoker>());
			Repaint();
		}

		private void OnEnable()
		{

		}

	}
}
#endif

