﻿using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
namespace Lib
{
    public class KeyInvokerInputWindow : EditorWindow
    {

        protected KeyCode _keyCode;

        private GUIStyle _yellow;

        private Vector2 _scrollPos;
        private List<UnityEventKeyInvoker> _keyInvokersAll = new List<UnityEventKeyInvoker>();
        protected UnityEventKeyInvoker _keyInvoker;

        [MenuItem("Window/KeyInvokerInputWindow")]
        public static void ShowWindow()
        {
            GetWindow<KeyInvokerInputWindow>("Key invoker input");
        }

        private void OnGUI()
        {
            if (_keyInvokersAll.Count == 0)
            {
                EditorGUILayout.LabelField("Not found key invoker input scripts in scene");
                return;
            }
            _scrollPos = EditorGUILayout.BeginScrollView(_scrollPos, GUILayout.Width(position.width), GUILayout.Height(position.height - 50f));
            foreach (var item in _keyInvokersAll)
            {
                EditorGUILayout.BeginHorizontal();
                _keyInvoker = EditorGUILayout.ObjectField(item, typeof(UnityEventKeyInvoker), GUILayout.Width(100)) as UnityEventKeyInvoker;
                StringBuilder stringBuilder = new StringBuilder();
                if (item.onDown != null)
                {
                    if (item.onDown.GetPersistentEventCount() > 0)
                    {
                        stringBuilder.Append(item.onDown.GetPersistentTarget(0).name);
                        stringBuilder.Append(".");
                        stringBuilder.Append(item.onDown.GetPersistentMethodName(0));
                    }
                    else
                    {
                        stringBuilder.Append("Empty");
                    }
                }
                else
                {
                    stringBuilder.Append("Error");
                }

                GUILayout.Label(stringBuilder.ToString());

                item.key = (KeyCode)EditorGUILayout.EnumPopup(item.key, GUILayout.Width(100f));
                UnityEventKeyInvoker unityEventKey = _keyInvokersAll.Find(x => x.key == item.key && x != item);
                if (unityEventKey)
                {
                    GUILayout.Label("!!!", _yellow);
                }

                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndScrollView();
        }

        private void OnInspectorUpdate()
        {
            _yellow = new GUIStyle(EditorStyles.label);
            _yellow.normal.textColor = Color.yellow;

            _keyInvokersAll.Clear();
            _keyInvokersAll.AddRange(FindObjectsOfType<UnityEventKeyInvoker>());
        }


    }
}
#endif
