using UnityEngine;

#if OVR

namespace Lib
{
    public class InputOVRRaycast : MonoBehaviour
	{
		public OVRInput.Controller inputController = OVRInput.Controller.RTouch;
		public OVRInput.Button inputButton = OVRInput.Button.PrimaryIndexTrigger;

		[SerializeField]
		private Raycast _raycast;

		private RaycastObject _raycastObject;

		public void StartInteracte()
		{
			if (_raycastObject)
			{
				_raycastObject.OnPointerRayDown();
			}
		}
		public void StopInteracte()
		{
			if (_raycastObject)
			{
				_raycastObject.OnPointerRayUp();
			}
		}

		private void Update()
		{
			if (OVRInput.GetDown(inputButton, inputController))
			{
				StartInteracte();
			}
			else if (OVRInput.GetUp(inputButton, inputController))
			{
				StopInteracte();
			}
		}

		private void OnEnable()
		{
			_raycast.OnHitRaycastObject += SetInteractableObject;
			_raycast.OnExitHitRaycast += RemoveInteractableObject;
		}

		private void RemoveInteractableObject(RaycastObject raycastObject)
		{
			if (_raycastObject)
			{
				if (OVRInput.Get(inputButton, inputController))
				{
					StopInteracte();
				}
			}
			_raycastObject = null;
		}

		private void OnDisable()
		{
			_raycast.OnHitRaycastObject -= SetInteractableObject;
			_raycast.OnExitHitRaycast -= RemoveInteractableObject;
		}

		private void SetInteractableObject(RaycastObject raycastObject)
		{
			_raycastObject = raycastObject;
		}
	}
}
#endif