using System;
using UnityEngine;

namespace Lib
{
	public class Raycast : MonoBehaviour
	{
		public const float MAX_DISTANCE = 200f;

		public event Action<GameObject> OnHitObject;
		public event Action<Rigidbody> OnHitRigidbidyObject;
		public event Action<RaycastObject> OnHitRaycastObject;

		public event Action<RaycastObject> OnExitHitRaycast;
		public event Action<RaycastObject> OnEnterHitRaycast;

		[SerializeField] protected LineRenderer _lineRenderer;
		[SerializeField] protected Transform _startPoint;
		[SerializeField] protected bool _raycasting = true;

		protected RaycastHit _hit;
		protected RaycastObject _lastRaycastObject;


		public virtual void DoRaycast(Vector3 startPosition, Vector3 forward)
		{
			_lineRenderer.SetPosition(0, startPosition);
			if (Physics.Raycast(startPosition, forward, out _hit, MAX_DISTANCE))
			{
				_lineRenderer.SetPosition(1, _hit.point);
				GetHitObject(_hit);
			}
			else
			{
				_lineRenderer.SetPosition(1, startPosition + (forward * MAX_DISTANCE));
				ExitLastRaycastObject();
				_lastRaycastObject = null;
			}
		}

		public RaycastObject GetRaycastObject(Collider collider)
		{
			RaycastObject raycastObject = collider.attachedRigidbody.GetComponent<RaycastObject>();
			if (raycastObject)
			{
				OnHitRaycastObject?.Invoke(raycastObject);
				if (_lastRaycastObject != raycastObject)
				{
					ExitLastRaycastObject();//if edge beetween two RayObjects
					OnEnterHitRaycast?.Invoke(raycastObject);
					raycastObject.OnPointerRaySelect(this);
				}
				_lastRaycastObject = raycastObject;
			}
			else
			{
				ExitLastRaycastObject();// if edge empty
				_lastRaycastObject = null;
			}
			return raycastObject;
		}

		public GameObject GetHitObject(RaycastHit hit)
		{
			
			if (hit.collider.attachedRigidbody)
			{
				OnHitRigidbidyObject?.Invoke(hit.collider.attachedRigidbody);
				GetRaycastObject(hit.collider);
			}
			OnHitObject?.Invoke(hit.collider.gameObject);
			return hit.collider.gameObject;
		}

		protected virtual void ExitLastRaycastObject()
		{
			if (_lastRaycastObject)// if edge empty
			{
				OnExitHitRaycast?.Invoke(_lastRaycastObject);
				_lastRaycastObject.OnPoinerRayExit(this);
			}
		}

		private void Update()
		{
			if (_raycasting)
			{
				DoRaycast(_startPoint.position, _startPoint.forward);
			}
		}
	}
}
