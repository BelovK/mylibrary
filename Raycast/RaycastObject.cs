using UnityEngine;
using UnityEngine.Events;

namespace Lib
{
	[RequireComponent(typeof(Rigidbody))]
	public class RaycastObject : MonoBehaviour
	{
		public UnityEvent OnInteract, OnStopInteract, OnSelect, OnDeselect;

		protected Rigidbody _rigidbody;

		public virtual void OnPointerRayDown()
		{
			OnInteract?.Invoke();
		}

		public virtual void OnPointerRayUp()
		{
			OnStopInteract.Invoke();
		}

		public virtual void OnPointerRaySelect(Raycast raycast)
		{
			OnSelect?.Invoke();
		}

		public virtual void OnPoinerRayExit(Raycast raycast)
		{
			OnDeselect?.Invoke();
		}

		private void Awake()
		{
			_rigidbody = GetComponent<Rigidbody>();
		}
	}
}
