using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Lib
{
    public static class Utils
    {
        public static bool TryGetComponentRB<T>(this Collider other, out T component)
        {
            bool result = other.attachedRigidbody.TryGetComponent(out T c);
            component = c;
            return result;
        }

        public static void DebugColor(string message, Color color)
        {
            Debug.Log("<color=" + color.FindColor(true) + ">" + message + "</color>");
        }
        public static void DebugColor(string message, Color color, GameObject gameObject)
        {
            Debug.Log("<color=" + color.FindColor(true) + ">" + message + "</color>", gameObject);
        }

        public static List<T> FindAll<T>()
        {
            List<T> interfaces = new List<T>();
            GameObject[] rootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();
            foreach (var rootGameObject in rootGameObjects)
            {
                T[] childrenInterfaces = rootGameObject.GetComponentsInChildren<T>();
                foreach (var childInterface in childrenInterfaces)
                {
                    interfaces.Add(childInterface);
                }
            }
            return interfaces;
        }

        public static IEnumerator LerpFloat(Action<float> callback, float startValue, float finalValue, float time, Action onDone = null)
        {
            float currentTime = 0;
            while (currentTime < time)
            {
                callback?.Invoke(Mathf.Lerp(startValue, finalValue, currentTime / time));
                yield return null;
                currentTime += Time.deltaTime;
            }
            callback?.Invoke(finalValue);
            onDone?.Invoke();
        }

        public static Coroutine InvokeRepeating(this MonoBehaviour monoBehaviour, Action action, float time)
        {
            return monoBehaviour.StartCoroutine(RepeatInvoke(time, action));
        }

        public static IEnumerator RepeatInvoke(float waitTime, Action callback)
        {
            WaitForSeconds waitForSeconds = new WaitForSeconds(waitTime);
            while (true)
            {
                yield return waitForSeconds;
                callback?.Invoke();
            }
        }

        public static Coroutine Invoke(this MonoBehaviour monoBehaviour, Action action, float delay, bool realTime = false)
        {
            return monoBehaviour.StartCoroutine(WaitAndInvoke(delay, action, realTime));
        }

        public static IEnumerator WaitAndInvoke(float waitTime, Action callback, bool realTime = false)
        {
            if (realTime)
            {
                yield return new WaitForSecondsRealtime(waitTime);
            }
            else
            {
                yield return new WaitForSeconds(waitTime);
            }
            callback?.Invoke();
        }
    }
}
