using UnityEngine;
using UnityEngine.Events;

namespace Lib
{
    [RequireComponent(typeof(Showable))]
    public class ShowableEventHandler : MonoBehaviour
    {
        public UnityEvent onShow, onHide;

        private Showable _showable;

        private void OnEnable()
        {
            _showable = GetComponent<Showable>();
            _showable.OnShow += RaiseOnShow;
            _showable.OnHide += RaiseOnHide;
        }

        private void OnDisable()
        {
            _showable.OnShow -= RaiseOnShow;
            _showable.OnHide -= RaiseOnHide;
        }

        private void RaiseOnHide()
        {
            onHide?.Invoke();
        }

        private void RaiseOnShow()
        {
            onShow?.Invoke();
        }
    }
}
