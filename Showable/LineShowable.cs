using System;
using System.Collections;
using UnityEngine;

namespace Lib
{
    public class LineShowable : Showable
    {
        [SerializeField] protected LineRenderer _lineRenderer;
        [SerializeField] protected Transform _startPoint;
        [SerializeField] protected Transform _endPoint;
        [SerializeField] protected float _time;

        protected Transform _point;
        protected Vector3 _oldPosition;
        protected bool _scaling;

        public void SetActive(bool value)
        {
            _lineRenderer.enabled = value;
        }

        public override void Hide(Action callback)
        {

        }

        public override void HideImmediately()
        {
            throw new NotImplementedException();
        }

        public override void Show(Action callback)
        {
            _lineRenderer.enabled = true;
            _lineRenderer.SetPosition(0, transform.InverseTransformPoint(_startPoint.position));
        }

        public override void ShowImmediately()
        {
            throw new NotImplementedException();
        }

        protected virtual IEnumerator ChangeShow(Action callback)
        {
            _scaling = true;
            yield return StartCoroutine(TransformExtension.LerpMove(_time, _point, transform.InverseTransformPoint(_startPoint.position), transform.InverseTransformPoint(_endPoint.position)));
            OnShowRaiseEvent();
            callback?.Invoke();
            _scaling = false;
        }

        protected virtual IEnumerator ChangeHide(Action callback)
        {
            _scaling = true;
            yield return StartCoroutine(TransformExtension.LerpMove(_time, _point, transform.InverseTransformPoint(_endPoint.position), transform.InverseTransformPoint(_startPoint.position)));
            OnHideRaiseEvent();
            callback?.Invoke();
            _scaling = false;
            SetActive(false);
        }

        protected virtual void Update()
        {
            if (!_lineRenderer.enabled)
            {
                return;
            }
            if (_oldPosition != transform.position)
            {
                _lineRenderer.SetPosition(0, transform.InverseTransformPoint(_startPoint.position));
                _oldPosition = transform.position;
            }
        }

        protected virtual void Awake()
        {
            _point = new GameObject("LinePoint").transform;
        }
    }
}
