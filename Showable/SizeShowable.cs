using Lib;
using System;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine;

public class SizeShowable : Showable
{
    [SerializeField] protected float _scaleTime = 1f;

    [SerializeField] protected bool _initOnAwake = true;
    [SerializeField] protected bool _hideOnAwake = true;
    [SerializeField] protected bool _disableObjectOnHide = true;

    protected float _startSize;
    protected bool _scaling;

    public void Init()
    {
        _startSize = transform.localScale.x;
    }

    public async void SetActive(bool value)
    {
        if (this == null || transform == null)
            return;
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(value);
            await Task.Yield();
            if (this == null || transform == null || transform.GetChild(i) == null)
                return;
        }
        IsVisible = value;
    }

    public override void Show(Action callback = null)
    {
        if (transform.localScale.x != 0)
        {
            return;
        }
        SetActive(true);
        StartCoroutine(ChangeShow(callback));
    }

    public override void Hide(Action callback = null)
    {
        if (transform.localScale.x != _startSize)
        {
            return;
        }
        StartCoroutine(ChangeHide(callback));
    }

    public override void HideImmediately()
    {
        StopAllCoroutines();
        _scaling = false;
        transform.localScale = Vector3.zero;
        OnHideRaiseEvent();
        SetActive(false);
    }

    public override void ShowImmediately()
    {
        StopAllCoroutines();
        _scaling = false;
        transform.localScale = new Vector3(_startSize, _startSize, _startSize);
        OnShowRaiseEvent();
        SetActive(true);
    }

    protected virtual IEnumerator ChangeShow(Action callback)
    {
        _scaling = true;
        yield return StartCoroutine(TransformExtension.LerpSize(_scaleTime, transform, 0, _startSize));
        OnShowRaiseEvent();
        callback?.Invoke();
        _scaling = false;
    }

    protected virtual IEnumerator ChangeHide(Action callback)
    {
        _scaling = true;
        yield return StartCoroutine(TransformExtension.LerpSize(_scaleTime, transform, _startSize, 0));
        OnHideRaiseEvent();
        callback?.Invoke();
        _scaling = false;
        if (_disableObjectOnHide)
            SetActive(false);
    }

    protected virtual void Awake()
    {
        if (_initOnAwake)
            Init();
        if (_hideOnAwake)
        {
            transform.localScale = Vector3.zero;
            SetActive(false);
        }
    }


}
