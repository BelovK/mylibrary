using System;
using UnityEngine;
using UnityEngine.UI;

namespace Lib
{
    public class DotImageShowable : Showable
    {
        public event Action<DotImageShowable> OnShowImage, OnHideImage, OnStartShowing, OnStartHideing;

        [SerializeField] private Showable _dot;
        [SerializeField] private Showable _image;
        [Header("View")]
        [SerializeField] private Button _button;

        public override void Hide(Action callback)
        {
            OnStartHideing?.Invoke(this);
            _image.Hide();
            _dot.Show(OnHideRaiseEvent);
        }

        public override void HideImmediately()
        {
            _image.HideImmediately();
            _dot.ShowImmediately();
            OnHideRaiseEvent();
        }

        public override void Show(Action callback)
        {
            OnStartShowing?.Invoke(this);
            _dot.Hide();
            _image.Show(OnShowRaiseEvent);
        }

        public override void ShowImmediately()
        {
            _image.ShowImmediately();
            _dot.HideImmediately();
            OnShowRaiseEvent();
        }

        protected override void OnShowRaiseEvent()
        {
            base.OnShowRaiseEvent();
            OnShowImage?.Invoke(this);
        }

        protected override void OnHideRaiseEvent()
        {
            base.OnHideRaiseEvent();
            OnHideImage?.Invoke(this);
        }


        private void OnEnable()
        {
            _button.onClick.AddListener(Show);
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(Show);
        }
    }
}
