using System;
using UnityEngine;

namespace Lib
{
    public class FastShowable : Showable
    {
        [SerializeField]
        protected GameObject _showableObject;

        public override void Hide(Action callback)
        {
            HideImmediately();
            callback?.Invoke();
        }

        public override void HideImmediately()
        {
            OnHideRaiseEvent();
            _showableObject.SetActive(false);
        }

        public override void Show(Action callback)
        {
            ShowImmediately();
            callback?.Invoke();
        }

        public override void ShowImmediately()
        {
            OnShowRaiseEvent();
            _showableObject.SetActive(true);
        }
    }
}
