﻿using System;
using UnityEngine;

namespace Lib
{
    public class ParticleShowable : Showable
    {
        [SerializeField] private ParticleSystem _particleSystem;
        public override void Hide(Action callback)
        {
            HideImmediately();
            callback?.Invoke();
        }

        public override void HideImmediately()
        {
            _particleSystem.Stop();
            IsVisible = false;
            OnHideRaiseEvent();
        }

        public override void Show(Action callback)
        {
            ShowImmediately();
            callback?.Invoke();
        }

        public override void ShowImmediately()
        {
            _particleSystem.Play();
            IsVisible = true;
            OnShowRaiseEvent();
        }
    }
}