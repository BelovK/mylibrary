using Lib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class ShowableGroup : Showable
{
    [SerializeField] private List<Showable> _showables = new List<Showable>();

    public async void ShowAsync(int timeBetween = 500, Action Callback = null)
    {
        foreach (Showable item in _showables)
        {
            if(item.gameObject.activeInHierarchy)
                item.Show();
            await Task.Delay(timeBetween);
        }
        Callback?.Invoke();
    }

    public async void HideAsync(int timeBetween = 500, Action Callback = null)
    {
        foreach (Showable item in _showables)
        {
            if (item.gameObject.activeInHierarchy)
                item.Hide();
            await Task.Delay(timeBetween);
        }
        Callback?.Invoke();
    }

    [ContextMenu("Find All showables in childs")]
    public void FindShowable()
    {
        _showables.Clear();
        _showables.AddRange(transform.GetComponentsInChildren<Showable>());
        _showables.RemoveAt(0);
    }

    public override void Hide(Action callback)
    {
        foreach (Showable item in _showables)
        {
            item.Hide();
        }
    }

    public override void HideImmediately()
    {
        foreach (Showable item in _showables)
        {
            item.HideImmediately();
        }
    }

    public override void Show(Action callback)
    {
        foreach (Showable item in _showables)
        {
            item.Show();
        }
    }

    public override void ShowImmediately()
    {
        foreach (Showable item in _showables)
        {
            item.ShowImmediately();
        }
    }
}
