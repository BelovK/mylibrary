using System;
using System.Collections;
using UnityEngine;

namespace Lib
{
    public class MoveShowable : Showable
    {
        [SerializeField]
        protected float _timeMoveUp = 1f;

        [SerializeField]
        protected Vector3 _plusPosition = Vector3.up;
        [SerializeField]
        protected Vector3 _plusUpRotation;


        protected Vector3 _startPosition;
        protected Quaternion _startRotation;

        protected Vector3 _upPosition;
        protected Quaternion _upRotation;


        protected bool _show;

        protected bool _moving;

        public override void Show(Action callback)
        {
            if (!_show && !_moving)
            {
                StartCoroutine(ShowDask(callback));
            }
        }
        [ContextMenu("Hide")]
        public override void Hide(Action callback)
        {
            if (_show && !_moving)
            {
                StartCoroutine(HideDask(callback));
            }
        }

        public override void ShowImmediately()
        {
            transform.position = _upPosition;
            transform.rotation = _upRotation;
            _show = true;
            OnShowRaiseEvent();
        }

        public override void HideImmediately()
        {
            transform.position = _startPosition;
            transform.rotation = _startRotation;
            _show = false;
            OnHideRaiseEvent();
        }

        protected virtual void Awake()
        {
            _startPosition = transform.position;
            _startRotation = transform.rotation;
            _upPosition = _startPosition + _plusPosition;
            _upRotation = _startRotation * Quaternion.Euler(_plusUpRotation);
            _show = false;
            _moving = false;
        }
        protected virtual IEnumerator ShowDask(Action callback)
        {
            _moving = true;
            this.LerpRotate(_timeMoveUp, transform, _startRotation, _upRotation, false);
            yield return this.LerpMove(_timeMoveUp, transform, _startPosition, _upPosition, false);
            callback?.Invoke();
            OnShowRaiseEvent();
            _moving = false;
            _show = true;
        }
        protected virtual IEnumerator HideDask(Action callback)
        {
            _moving = true;
            this.LerpRotate(_timeMoveUp, transform, transform.rotation, _startRotation, false);
            yield return this.LerpMove(_timeMoveUp, transform, transform.position, _startPosition, false);
            callback?.Invoke();
            OnHideRaiseEvent();
            _show = false;
            _moving = false;
        }


    }
}
