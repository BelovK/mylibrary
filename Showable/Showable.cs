using System;
using UnityEngine;

namespace Lib
{
    public abstract class Showable : MonoBehaviour
    {
        public event Action OnShow, OnHide;

        public bool IsVisible { get; protected set; }

        [ContextMenu("Show")]
        public void Show()
        {
            Show(null);
        }
        [ContextMenu("Hide")]
        public void Hide()
        {
            Hide(null);
        }

        public bool IsActive()
        {
            return transform.GetChild(0).gameObject.activeInHierarchy;
        }

        public abstract void Show(Action callback);
        public abstract void Hide(Action callback);

        public abstract void ShowImmediately();
        public abstract void HideImmediately();

        protected virtual void OnShowRaiseEvent()
        {
            OnShow?.Invoke();
        }

        protected virtual void OnHideRaiseEvent()
        {
            OnHide?.Invoke();
        }
    }
}