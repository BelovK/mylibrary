using System;
using System.Collections;
using UnityEngine;

namespace Lib
{
    public class SizeCurveShowable : SizeShowable
    {
        [SerializeField] protected AnimationCurve _sizeCurve;

        protected override IEnumerator ChangeShow(Action callback)
        {
            _scaling = true;
            yield return this.LerpSize(_scaleTime, transform, _sizeCurve, true);
            OnShowRaiseEvent();
            callback?.Invoke();
            _scaling = false;
        }

        protected override IEnumerator ChangeHide(Action callback)
        {
            _scaling = true;
            yield return this.LerpSize(_scaleTime, transform, _sizeCurve, false);
            if (_disableObjectOnHide)
                SetActive(false);
            OnHideRaiseEvent();
            callback?.Invoke();
            _scaling = false;
            
            
        }

        protected virtual void OnDisable()
        {
            StopAllCoroutines();
        }

    }
}