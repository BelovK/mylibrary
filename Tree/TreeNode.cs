using System;
using System.Collections.Generic;
using UnityEngine;

namespace Lib
{
    [Serializable]
    public class TreeNode<T>
    {
        [SerializeField] private T _data;
        [SerializeField] private List<TreeNode<T>> _children;

        public TreeNode(T data)
        {
            this._data = data;
            _children = new List<TreeNode<T>>();
        }

        public void AddChild(T data)
        {
            _children.Add(new TreeNode<T>(data));
        }

        public TreeNode<T> GetChild(int i)
        {
            foreach (TreeNode<T> n in _children)
                if (--i == 0)
                    return n;
            return null;
        }

        public void Traverse(TreeNode<T> node, Action<T> visitor)
        {
            visitor(node._data);
            foreach (TreeNode<T> kid in node._children)
                Traverse(kid, visitor);
        }
    }
}