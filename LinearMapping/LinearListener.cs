using UnityEngine;
using UnityEngine.Events;

namespace Lib
{
	public class LinearListener : MonoBehaviour
	{
		public const float VALUE_LIMIT_SENSITIVITY = 0.04f;

		public UnityEvent OnMax, OnMin;

		[SerializeField] private LinearMapping _linearMapping;
		private float _oldValue;

		public void CheckValue(float value)
		{
			if (_oldValue < 1 - VALUE_LIMIT_SENSITIVITY && value > 1 - VALUE_LIMIT_SENSITIVITY)
			{
				OnMax.Invoke();
			}
			else if (_oldValue > VALUE_LIMIT_SENSITIVITY && value < VALUE_LIMIT_SENSITIVITY)
			{
				OnMin.Invoke();
			}
			_oldValue = value;
		}

		private void OnEnable()
		{
			_linearMapping.OnValueChanged += CheckValue;
		}

		private void OnDisable()
		{
			_linearMapping.OnValueChanged -= CheckValue;
		}
	}
}
