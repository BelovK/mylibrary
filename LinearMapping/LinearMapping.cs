﻿using System;
using UnityEngine;

namespace Lib
{
	public class LinearMapping : MonoBehaviour
	{
		public float Value
		{
			get
			{
				return _value;
			}
			set
			{
				if (_value != value)
				{
					_value = value;
					OnValueChanged?.Invoke(_value);
				}
			}
		}
		[SerializeField] private float _value;

		public event Action<float> OnValueChanged;
	}
}
