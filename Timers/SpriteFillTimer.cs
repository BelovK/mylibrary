using UnityEngine;
using UnityEngine.UI;

namespace Lib
{
    public class SpriteFillTimer : Timer
    {
        [SerializeField] protected Image _image;
        [SerializeField] protected bool _inverse = false;
        public virtual void UpdateFill(float time)
        {
            if (_inverse)
            {
                _image.fillAmount = (_startTime - time) / _startTime;
            }
            else
            {
                _image.fillAmount = time / _startTime;
            }
            
        }

        protected virtual void OnEnable()
        {
            OnTimerTick += UpdateFill;
        }

        protected virtual void OnDisable()
        {
            OnTimerTick -= UpdateFill;
        }
    }
}