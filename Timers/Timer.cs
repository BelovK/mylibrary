using System;
using System.Threading.Tasks;
using UnityEngine;

namespace Lib
{
    public class Timer : MonoBehaviour
    {
        public bool IsTicking => _currentTime != 0;

        public event Action<float> OnStartTimer;
        public event Action<float> OnTimerTick;
        public event Action<float> OnDone;

        [SerializeField] private float _currentTime = 0;

        protected float _startTime;

        [ContextMenu("Start")]
        public void StartTimer()
        {
            if (!IsTicking)
            {
                StartTimer(_currentTime);
            }
            else
            {
                Debug.LogWarning("Timer already started");
            }
        }

        public async void StartTimer(float sec, bool cooldown = true)
        {
            _startTime = sec;
            if (cooldown)
            {
                _currentTime = sec;
                OnStartTimer?.Invoke(_currentTime);
                while (_currentTime > 0)
                {
                    await Task.Yield();
                    _currentTime -= Time.deltaTime;
                    OnTimerTick?.Invoke(_currentTime);
                }
                _currentTime = 0;
            }
            else
            {
                _currentTime = 0;
                OnStartTimer?.Invoke(_currentTime);
                while (_currentTime < _startTime)
                {
                    await Task.Yield();
                    _currentTime += Time.deltaTime;
                    OnTimerTick?.Invoke(_currentTime);
                }
                _currentTime = _startTime;
            }
            await Task.Yield();
            OnTimerTick?.Invoke(_currentTime);
            OnDone?.Invoke(_currentTime);
            _currentTime = 0;
        }

        protected virtual void Start()
        {
            _currentTime = 0;
        }
    }
}