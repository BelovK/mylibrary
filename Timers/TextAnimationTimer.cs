using UnityEngine;

namespace Lib
{
    public class TextAnimationTimer : TextMeshTimer
    {
        public const string TRIGGER_NAME = "Play";

        [Header("Required animation with trigger play")]
        [SerializeField] private Animator _animator;

        private int _oldTime;

        public override void UpdateText(float time)
        {
            base.UpdateText(time);
            int currentDivRanks = (int)(time % 10);
            if (currentDivRanks != _oldTime)
            {
                _animator.SetTrigger(TRIGGER_NAME);//FIX
                Debug.Log(_oldTime);
            }
            _oldTime = currentDivRanks;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            OnDone += HideTimer;
            OnStartTimer += ShowTimer;
        }

        private void ShowTimer(float time)
        {
            _textMeshPro.enabled = true;
        }

        private void HideTimer(float endTime)
        {
            _textMeshPro.enabled = false;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            OnDone -= HideTimer;
            OnStartTimer -= ShowTimer;
        }
    }
}
