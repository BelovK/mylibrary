using TMPro;
using UnityEngine;

namespace Lib
{
    public class TextMeshTimer : Timer
    {
        [SerializeField] protected TMP_Text _textMeshPro;
        [SerializeField] private string _format = "#";

        public virtual void UpdateText(float time)
        {
            _textMeshPro.text = time.ToString(_format);
        }

        protected virtual void OnEnable()
        {
            OnTimerTick += UpdateText;
        }

        protected virtual void OnDisable()
        {
            OnTimerTick -= UpdateText;
        }
    }
}
