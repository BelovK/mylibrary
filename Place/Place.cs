using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lib
{
	[RequireComponent(typeof(Rigidbody))]
	public abstract class Place<T> : MonoBehaviour where T : MonoBehaviour
	{
		[field: SerializeField]
		public T Item { get; private set; }

		[field: SerializeField]
		public T PotentialItem { get; private set; }

		public event Action<T> OnSetItem;
		public event Action<T> OnReleaseItem;
		public event Action<T> OnEnterItem;
		public event Action<T> OnExitItem;

		//Colliders
		protected Collider _lastCollider;

		public virtual void SetItem(T item)
		{
			Item = PotentialItem = item;
			OnSetItem?.Invoke(Item);
		}

		public virtual void ReleaseItem()
		{
			OnSetItem?.Invoke(Item);
			Item = PotentialItem = null;
		}

		public virtual bool EnterItem(T item)
		{
            if (PotentialItem != null)
            {
                return false;
            }
            PotentialItem = item;
			OnEnterItem?.Invoke(PotentialItem);
			return true;
		}

		public virtual bool ExitItem(T item)
		{
            if (!PotentialItem)
            {
                Debug.Log("Return exit");
				return false;
			}
            if (PotentialItem == item && !Item)
			{
				OnExitItem?.Invoke(PotentialItem);
				PotentialItem = null;
				return true;
			}
			return false;
		}

		protected void OnTriggerEnter(Collider collider)
		{
			if (!collider.attachedRigidbody)
			{
				return;
			}
			if (collider.attachedRigidbody.TryGetComponent<T>(out T item))
			{
				_lastCollider = collider;
				EnterItem(item);
			}
		}
		protected void OnTriggerExit(Collider collider)
		{
			if (!collider.attachedRigidbody)
			{
				return;
			}
			if (collider.attachedRigidbody.TryGetComponent<T>(out T item) && _lastCollider == collider)
			{
				ExitItem(item);
				_lastCollider = null;
			}
		}

	}
}
