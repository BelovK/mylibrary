using System.Collections;
using UnityEngine;

namespace Lib
{
    public static class TransformExtension
    {
        public static void ResetTransform(this Transform transform, Transform parent)
        {
            transform.SetParent(parent);
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        }

        #region Size

        public static Coroutine LerpSize(this MonoBehaviour mono, float time, Transform sizeTransform, float startSize, float finalSize)
        {
            return mono.StartCoroutine(LerpSize(time, sizeTransform, startSize, finalSize));
        }
        public static Coroutine LerpSize(this MonoBehaviour mono, float time, Transform sizeTransform, AnimationCurve animationCurve, bool increase = true)
        {
            return mono.StartCoroutine(LerpSize(time, sizeTransform, animationCurve, increase));
        }

        public static IEnumerator LerpSize(float time, Transform sizeTransform, float startSize, float finalSize)
        {
            float currentTime = 0;
            Vector3 startVector = new Vector3(startSize, startSize, startSize);
            Vector3 endVector = new Vector3(finalSize, finalSize, finalSize);
            while (currentTime < time)
            {
                sizeTransform.localScale = Vector3.Lerp(startVector, endVector, currentTime / time);
                yield return null;
                currentTime += Time.deltaTime;
            }
            sizeTransform.localScale = endVector;
        }

        public static IEnumerator LerpSize(float time, Transform sizeTransform, AnimationCurve animationCurve, bool increase = true)
        {
            float currentTime = 0;
            float size;
            while (currentTime <= time)
            {
                if (increase)
                {
                    size = animationCurve.Evaluate(currentTime / time);
                }
                else
                {
                    size = animationCurve.Evaluate((time - currentTime) / time);
                }
                sizeTransform.localScale = new Vector3(size, size, size);
                yield return null;
                currentTime += Time.deltaTime;
            }
            currentTime = time;
            if (increase)
            {
                size = animationCurve.Evaluate(currentTime / time);
            }
            else
            {
                size = animationCurve.Evaluate((time - currentTime) / time);
            }
            sizeTransform.localScale = new Vector3(size, size, size);
        }
        #endregion

        #region Position
        public static Coroutine LerpMove(this MonoBehaviour mono, float time, Transform moveTransform, Vector3 startPosition, Vector3 endPosition, bool isLocal = true)
        {
            return mono.StartCoroutine(LerpMove(time, moveTransform, startPosition, endPosition, isLocal));
        }

        public static Coroutine LerpMove(this MonoBehaviour mono, float time, Transform moveTransform, Vector3 startPosition, Vector3 endPosition, AnimationCurve accelerationCurve, bool isLocal = true)
        {
            return mono.StartCoroutine(AcceleratedMove(time, moveTransform, startPosition, endPosition, accelerationCurve, isLocal));
        }

        /// <summary>
        /// Move transform form startPosition to endPosition in local or world space in "time" time :-)
        /// </summary>
        /// <param name="time"></param>
        /// <param name="moveTransform"></param>
        /// <param name="startPosition"></param>
        /// <param name="endPosition"></param>
        /// <param name="isLocal"></param>
        /// <returns></returns>
        public static IEnumerator LerpMove(float time, Transform moveTransform, Vector3 startPosition, Vector3 endPosition, bool isLocal = true)
        {
            float currentTime = 0;
            while (currentTime < time)
            {
                Vector3 lerpResult = Vector3.Lerp(startPosition, endPosition, currentTime / time);
                if (isLocal)
                {
                    moveTransform.localPosition = lerpResult;
                }
                else
                {
                    moveTransform.position = lerpResult;
                }

                yield return null;
                currentTime += Time.deltaTime;
            }
            if (isLocal)
            {
                moveTransform.localPosition = endPosition;
            }
            else
            {
                moveTransform.position = endPosition;
            }
        }

        public static IEnumerator AcceleratedMove(float time, Transform moveTransform, Vector3 startPosition, Vector3 endPosition, AnimationCurve accelerationCurve, bool isLocal = true)
        {
            float currentTime = 0;
            while (currentTime < time)
            {
                Vector3 lerpResult = Vector3.Lerp(startPosition, endPosition, accelerationCurve.Evaluate(currentTime / time));
                if (isLocal)
                {
                    moveTransform.localPosition = lerpResult;
                }
                else
                {
                    moveTransform.position = lerpResult;
                }

                yield return null;
                currentTime += Time.deltaTime;
            }
            if (isLocal)
            {
                moveTransform.localPosition = endPosition;
            }
            else
            {
                moveTransform.position = endPosition;
            }
        }

        public static IEnumerator LerpMoveDistance(float distanceMove, Transform moveTransform, Vector3 startPosition, Vector3 endPosition)
        {
            float currentDistance = Vector3.Distance(startPosition, endPosition);
            while (currentDistance >= distanceMove * Time.deltaTime)
            {
                currentDistance = Vector3.Distance(moveTransform.position, endPosition);
                moveTransform.position = Vector3.MoveTowards(moveTransform.position, endPosition, distanceMove * Time.deltaTime);
                yield return null;
            }
            moveTransform.position = endPosition;
        }
        #endregion

        #region Rotate
        public static Coroutine LerpRotateAngle(this MonoBehaviour mono, float rotateAngle, Transform rotateTransform, Quaternion startRotation, Quaternion endRotation, bool isLocal = true)
        {
            return mono.StartCoroutine(LerpRotateAngle(rotateAngle, rotateTransform, startRotation, endRotation, isLocal));
        }

        public static Coroutine LerpRotate(this MonoBehaviour mono, float time, Transform rotateTransform, Quaternion startRotation, Quaternion endRotation, bool isLocal = true)
        {
            return mono.StartCoroutine(LerpRotate(time, rotateTransform, startRotation, endRotation, isLocal));
        }

        public static IEnumerator LerpRotateAngle(float rotateAngle, Transform rotateTransform, Quaternion startRotation, Quaternion endRotation, bool isLocal = true)
        {
            float currentAngle = Quaternion.Angle(startRotation, endRotation);
            while (currentAngle >= rotateAngle * Time.deltaTime)
            {
                currentAngle = Quaternion.Angle(rotateTransform.rotation, endRotation);
                rotateTransform.rotation = Quaternion.RotateTowards(rotateTransform.rotation, endRotation, rotateAngle * Time.deltaTime);
                yield return null;
            }
            rotateTransform.rotation = endRotation;
        }

        public static IEnumerator LerpRotate(float time, Transform rotateTransform, Quaternion startRotation, Quaternion endRotation, bool isLocal = true)
        {
            float currentTime = 0;
            while (currentTime < time)
            {
                Quaternion lerpResult = Quaternion.Lerp(startRotation, endRotation, currentTime / time);
                if (isLocal)
                {
                    rotateTransform.localRotation = lerpResult;
                }
                else
                {
                    rotateTransform.rotation = lerpResult;
                }

                yield return null;
                currentTime += Time.deltaTime;
            }
            if (isLocal)
            {
                rotateTransform.localRotation = endRotation;
            }
            else
            {
                rotateTransform.rotation = endRotation;
            }
        }
        #endregion
    }
}
