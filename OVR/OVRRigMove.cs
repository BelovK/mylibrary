﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if OVR
using OVR;

namespace Lib
{
	public class OVRRigMove : MonoBehaviour
	{
		public OVRInput.Axis2D moveDirection;
		public float speed = 1f;
		public Transform normal;

		private Rigidbody rigidbody;
		// Start is called before the first frame update
		void Start()
		{
			rigidbody = GetComponent<Rigidbody>();
		}

		// Update is called once per frame
		void Update()
		{
			Vector2 inputVector = OVRInput.Get(moveDirection);
			rigidbody.MovePosition(rigidbody.position + (((normal.forward * inputVector.y) + (normal.right * inputVector.x)) * speed));

		}
	}
}
#endif