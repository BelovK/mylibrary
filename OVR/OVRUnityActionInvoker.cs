﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if OVR
using OVR;
using UnityEngine.Events;
namespace Lib
{
	public enum PressType { hold, down, up, doubleClick }

	public class OVRUnityActionInvoker : MonoBehaviour
	{
		public OVRInput.Button button;
		public OVRInput.Controller controller;
		public PressType pressType = PressType.down;


		public UnityEvent onClick;

		private float _doubleClickTime = 0.3f;
		private int _clickCounter = 0;

		private IEnumerator _doubleClickCortune;


		public void Invoke()
		{
			onClick.Invoke();
		}

		private void Update()
		{
			switch (pressType)
			{
				case PressType.hold:
					if (OVRInput.Get(button, controller))
					{
						Invoke();
					}
					break;
				case PressType.down:
					if (OVRInput.GetDown(button, controller))
					{
						Invoke();
					}
					break;
				case PressType.up:
					if (OVRInput.GetUp(button, controller))
					{
						Invoke();
					}
					break;
				case PressType.doubleClick:
					if (OVRInput.GetDown(button, controller))
					{
						_doubleClickCortune = DoubleClick();
						StartCoroutine(_doubleClickCortune);
					}
					break;
			}
		}

		private IEnumerator DoubleClick()
		{
			_clickCounter++;
			if (_clickCounter > 1)
			{
				Debug.Log("DoubleClick");
				Invoke();
			}
			yield return new WaitForSeconds(_doubleClickTime);
			_clickCounter = 0;

		}
	}
}
#endif
