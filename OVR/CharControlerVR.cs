﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if OVR
namespace Lib
{
	[RequireComponent(typeof(CharacterController))]
	public class CharControlerVR : MonoBehaviour
	{

		public OVRInput.Axis2D moveAction;

		public float speed = 2f;

		public Transform normal;

		private CharacterController _characterController;
		private bool _keybord = true;
		private Vector2 _inputVector;

		const string yAxisName = "Vertical";
		const string xAxisName = "Horizontal";

		float SimulationRate = 0.1f;
		float FallSpeed = 0f;

		// Start is called before the first frame update
		void OnEnable()
		{
			_characterController = GetComponent<CharacterController>();
			_keybord = true;
		}

		// Update is called once per frame
		void FixedUpdate()
		{
			//_inputVector = OVRInput.Get(moveAction);
			////_characterController.Move((normal.forward * m.y) + (normal.right * m.x));
			//_characterController.SimpleMove(((normal.forward * _inputVector.y) + (normal.right * _inputVector.x)) * speed);
			if (Input.GetKeyDown(KeyCode.Keypad0))
			{
				_keybord = !_keybord;
			}

			if (_keybord)
			{
				_inputVector = OVRInput.Get(moveAction);
				if (_characterController.isGrounded && FallSpeed <= 0)
					FallSpeed = Physics.gravity.y;
				else
					FallSpeed += Physics.gravity.y * SimulationRate * Time.deltaTime;
				//transform.position += ((normal.forward * _inputVector.y) + (normal.right * _inputVector.x)) * speed * 0.5f;
				Vector3 m = ((normal.forward * _inputVector.y) + (normal.right * _inputVector.x)) * speed;
				m.y = FallSpeed;
				if (_inputVector != Vector2.zero)
					_characterController.Move(m * Time.fixedDeltaTime);
				//Vector3 movement = ((Vector3.ProjectOnPlane(normal.forward, Vector3.up) * Input.GetAxis(yAxisName)) + ((Vector3.ProjectOnPlane(normal.right, Vector3.up) * Input.GetAxis(xAxisName)))).normalized;
				//if (movement != Vector3.zero)
				//{
				//	_characterController.SimpleMove(movement * speed);
				//}
			}
			else
			{
				_inputVector = OVRInput.Get(moveAction);
				//_characterController.Move((normal.forward * m.y) + (normal.right * m.x));


				if (_inputVector != Vector2.zero)
					_characterController.SimpleMove(((normal.forward * _inputVector.y) + (normal.right * _inputVector.x)) * speed);
			}
		}
	}
}
#endif
