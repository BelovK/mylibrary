﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if OVR
using OVR;
namespace Lib
{
	public class JoystickRotationObject : MonoBehaviour
	{
		public float speed = 1f;
		private Vector3 _inputVector;

		private void FixedUpdate()
		{
			_inputVector = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick);
			if (_inputVector.magnitude > 0.01f)
			{
				transform.localRotation *= Quaternion.Euler(-_inputVector.y * speed, -_inputVector.x * speed, 0f);
			}
		}
	}
}
#endif
