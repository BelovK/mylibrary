﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if OVR
using OVR;

namespace Lib
{
	public class OVRFreeFly : MonoBehaviour
	{
		[SerializeField]
		protected Transform _moveRelativelyTransform;

		public float Speed { get; set; } = 0.5f;

		public OVRInput.Controller Controller { get; set; } = OVRInput.Controller.RTouch;

		private Vector2 _moveVector;

		// Update is called once per frame
		void Update()
		{
			_moveVector = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, Controller);
			if (_moveVector.magnitude > 0.01f)
			{
				transform.position += ((_moveRelativelyTransform.forward * _moveVector.y) + (_moveRelativelyTransform.right * _moveVector.x)) * Speed;
			}
		}
	}
}
#endif
