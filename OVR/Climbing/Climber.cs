﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if OVR
namespace Lib
{
	[RequireComponent(typeof(Rigidbody))]
	public class Climber : MonoBehaviour
	{
		public ClimbingHand RightHand;
		public ClimbingHand LeftHand;
		public OVRInput.Button rightButton;
		public OVRInput.Button leftButton;
		//public SteamVR_Action_Boolean ToggleGripButton;
		//public SteamVR_Action_Pose position;
		public ConfigurableJoint ClimberHandle;
		public Vector3 center;

		private bool Climbing;
		private ClimbingHand ActiveHand;

		void Update()
		{
			updateHand(RightHand);
			updateHand(LeftHand);
			if (Climbing)
			{
				ClimberHandle.targetPosition = -ActiveHand.transform.localPosition;//update collider for hand movment
			}
		}

		void updateHand(ClimbingHand Hand)
		{
			if (Climbing && Hand == ActiveHand)//if is the hand used for climbing check if we are letting go.
			{
				if (Hand.Hand == OVRInput.Controller.LTouch)
				{
					if (OVRInput.GetUp(leftButton))
					{
						ClimberHandle.connectedBody = null;
						Climbing = false;

						GetComponent<Rigidbody>().useGravity = true;
					}
				}
				else if (Hand.Hand == OVRInput.Controller.RTouch)
				{
					if (OVRInput.GetUp(rightButton))
					{
						ClimberHandle.connectedBody = null;
						Climbing = false;

						GetComponent<Rigidbody>().useGravity = true;
					}
				}
				//if (OVRInput.GetUp(rightButton) || OVRInput.GetUp(leftButton))
				//{
				//	ClimberHandle.connectedBody = null;
				//	Climbing = false;

				//	GetComponent<Rigidbody>().useGravity = true;
				//}
			}
			else
			{
				if (Hand.Hand == OVRInput.Controller.LTouch)
				{
					if (OVRInput.GetDown(leftButton) || Hand.grabbing)
					{
						Hand.grabbing = true;
						if (Hand.TouchedCount > 0)
						{
							ActiveHand = Hand;
							Climbing = true;
							ClimberHandle.transform.position = Hand.transform.position;
							GetComponent<Rigidbody>().useGravity = false;
							ClimberHandle.connectedBody = GetComponent<Rigidbody>();
							Hand.grabbing = false;
						}
					}
				}
				else
				if (OVRInput.GetDown(rightButton) || Hand.grabbing)
				{
					Hand.grabbing = true;
					if (Hand.TouchedCount > 0)
					{
						ActiveHand = Hand;
						Climbing = true;
						ClimberHandle.transform.position = Hand.transform.position;
						GetComponent<Rigidbody>().useGravity = false;
						ClimberHandle.connectedBody = GetComponent<Rigidbody>();
						Hand.grabbing = false;
					}
				}
			}
		}
	}
}
#endif