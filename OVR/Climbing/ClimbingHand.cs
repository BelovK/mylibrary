﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if OVR
using OVR;

namespace Lib
{

	public class ClimbingHand : MonoBehaviour
	{
		public OVRInput.Controller Hand;
		public int TouchedCount;
		public bool grabbing;

		void OnTriggerEnter(Collider other)
		{
			if (other.CompareTag("Climbable"))
			{
				TouchedCount++;
			}
		}
		void OnTriggerExit(Collider other)
		{
			if (other.CompareTag("Climbable"))
			{
				TouchedCount--;
			}
		}
	}
}
#endif
