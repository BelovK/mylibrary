using System.Collections;
using UnityEngine;
using System;
using UnityEngine.EventSystems;

#if OVR

using UnityEngine.XR.Management;
using UnityEngine.Events;

namespace Lib
{
	public class SwitchXRMode : MonoBehaviour
	{
		public const string NAME_XR_ACTIVE_PREF = "XRmode";
		public const float TIME_BEFORE_INIT = 0.1f;

		public static event Action<bool> OnChangedXRMode;

		public static bool GlobalEnable
		{
			get
			{
				if (PlayerPrefs.GetInt(NAME_XR_ACTIVE_PREF) == 1)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			set
			{
				OnChangedXRMode?.Invoke(value);
				PlayerPrefs.SetInt(NAME_XR_ACTIVE_PREF, value ? 1 : 0);
			}
		}

		public UnityEvent onXRActivated, onXRDisabled;

		public bool Active { get => _active; }
		private bool _active = true;
		[Header("For better performance assign it in inspector")]
		[Tooltip("For better performance assign in inspector")]
		[SerializeField] 
		protected EventSystem _eventSystem;
		[Tooltip("For better performance assign in inspector")]
		[SerializeField] 
		protected EventSystem _eventSystemVR;

		public virtual void InvokeXRActivated()
		{
			onXRActivated?.Invoke();
		}

		public virtual void InvokeXRDisabled()
		{
			onXRDisabled?.Invoke();
		}

		public void SwitchActive()
		{
			_active = !_active;
			SetActiveVR(_active);
		}

		public void SetActiveVR(bool value)
		{
			if (value)
			{
				StartCoroutine(StartXR());
			}
			else
			{
				StopXR();
			}
			GlobalEnable = _active = value;
		}

		private IEnumerator StartXR()
		{
			Debug.Log("Initializing XR...");
			yield return XRGeneralSettings.Instance.Manager.InitializeLoader();

			Debug.Log("Starting XR...");
			XRGeneralSettings.Instance.Manager.StartSubsystems();
			InvokeXRActivated();

			_eventSystem.gameObject.SetActive(false);
			_eventSystemVR.gameObject.SetActive(true);
		}

		private void StopXR()
		{
			Debug.Log("Stopping XR...");

			XRGeneralSettings.Instance.Manager.StopSubsystems();
			XRGeneralSettings.Instance.Manager.DeinitializeLoader();
			Camera.main.fieldOfView = 70f;

			Debug.Log("XR stopped completely.");

			InvokeXRDisabled();

			_eventSystem.gameObject.SetActive(true);
			_eventSystemVR.gameObject.SetActive(false);
		}

		private void OnEnable()
		{
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
			StartCoroutine(InitXRMode());
#else
		if (_eventSystemVR == null)
		{
			_eventSystemVR = Utils.FindAll<IEventSystemVR>()[0].GetGameObject().GetComponent<EventSystem>();// BULLSHIT
		}
		StartCoroutine(StartXR());
#endif
		}

		private IEnumerator InitXRMode()
		{
			//For better performance assign in inspector
			if (_eventSystem == null)
			{
				_eventSystem = Utils.FindAll<EventSystem>().Find(x => x.GetComponent<IEventSystemVR>() == null);
			}
			if (_eventSystemVR == null)
			{
				_eventSystemVR = Utils.FindAll<IEventSystemVR>()[0].GetGameObject().GetComponent<EventSystem>();// BULLSHIT
			}

			yield return new WaitForSeconds(TIME_BEFORE_INIT);

			SetActiveVR(GlobalEnable);
		}
	}
}
#endif