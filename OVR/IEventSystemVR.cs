using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEventSystemVR
{
	public GameObject GetGameObject();
}
