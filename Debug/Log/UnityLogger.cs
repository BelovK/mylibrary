using UnityEngine;

namespace Lib
{
    public class UnityLogger : ILogger
    {
        public void Log(string className, string message)
        {
            Debug.Log($"[{className}] {message}");
        }

        public void LogWarning(string className, string message)
        {
            Debug.LogWarning($"[{className}] {message}");
        }

        public void LogError(string className, string message)
        {
            Debug.LogError($"[{className}] {message}");
        }
    }
}
