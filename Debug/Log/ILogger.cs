﻿namespace Lib
{
    public interface ILogger
    {
        void Log(string className, string message);
        void LogError(string className, string message);
        void LogWarning(string className, string message);
    }
}