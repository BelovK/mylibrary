﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using TMPro;

namespace Lib
{

	/// <summary>
	/// Only for EDITOR!!!
	/// </summary>
	public class BatchesInfo : MonoBehaviour
	{
		[Header("Only for EDITOR!!!")]
		public TextMeshProUGUI textBatches;
		public TextMeshProUGUI textDynamicBatchedDrawCalls;
		public TextMeshProUGUI textStaticBatchedDrawCalls;
		public TextMeshProUGUI textInstancedBatchedDrawCalls;
		public TextMeshProUGUI textDynamicBatched;
		public TextMeshProUGUI textStaticBatched;
		public TextMeshProUGUI textInstancedBatches;

		private void Start()
		{
			if (!Application.isEditor)
			{
				textBatches.gameObject.SetActive(false);
				textDynamicBatchedDrawCalls.gameObject.SetActive(false);
				textStaticBatchedDrawCalls.gameObject.SetActive(false);
				textInstancedBatchedDrawCalls.gameObject.SetActive(false);
				textDynamicBatched.gameObject.SetActive(false);
				textStaticBatched.gameObject.SetActive(false);
				textInstancedBatches.gameObject.SetActive(false);
			}
		}

#if UNITY_EDITOR
		// Update is called once per frame
		void Update()
		{
			if (textBatches)
			{
				textBatches.text = "batches: " + UnityStats.batches.ToString();
			}
			if (textDynamicBatchedDrawCalls)
			{
				textDynamicBatchedDrawCalls.text = "dynamicBDC: " + UnityStats.dynamicBatchedDrawCalls.ToString();
			}
			if (textStaticBatchedDrawCalls)
			{
				textStaticBatchedDrawCalls.text = "staticBDC: " + UnityStats.staticBatchedDrawCalls.ToString();
			}
			if (textInstancedBatchedDrawCalls)
			{
				textInstancedBatchedDrawCalls.text = "instancedBDC: " + UnityStats.instancedBatchedDrawCalls.ToString();
			}
			if (textDynamicBatched)
			{
				textDynamicBatched.text = "dynamicBatches: " + UnityStats.dynamicBatches.ToString();
			}
			if (textStaticBatched)
			{
				textStaticBatched.text = "staticBatches: " + UnityStats.staticBatches.ToString();
			}
			if (textInstancedBatches)
			{
				textInstancedBatches.text = "instancedBatches: " + UnityStats.instancedBatches.ToString();
			}
		}
#endif
	}
}