﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Lib
{
	public class FPSCounter : MonoBehaviour
	{
		[SerializeField] private float _frequency = 0.5f;
		[SerializeField] private TextMeshProUGUI _fpsText;

		public int FramesPerSec { get; protected set; }

		private void Start()
		{
			StartCoroutine(FPS());
		}

		private IEnumerator FPS()
		{
			for (; ; )
			{
				// Capture frame-per-second
				int lastFrameCount = Time.frameCount;
				float lastTime = Time.realtimeSinceStartup;
				yield return new WaitForSeconds(_frequency);
				float timeSpan = Time.realtimeSinceStartup - lastTime;
				int frameCount = Time.frameCount - lastFrameCount;

				// Display it
				FramesPerSec = Mathf.RoundToInt(frameCount / timeSpan);
				_fpsText.text = FramesPerSec.ToString() + " fps";
			}
		}
	}
}
