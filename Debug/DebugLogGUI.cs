using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class DebugLogGUI : MonoBehaviour
{
    [SerializeField] private int maxLines = 8;
    private Queue<string> _queue = new Queue<string>();
    private string _currentText = "";

    private void OnEnable()
    {
        Application.logMessageReceivedThreaded += HandleLog;
    }

    private void OnDisable()
    {
        Application.logMessageReceivedThreaded -= HandleLog;
    }

    private void HandleLog(string logString, string stackTrace, LogType type)
    {
        if (_queue.Count >= maxLines) _queue.Dequeue();

        _queue.Enqueue(logString);

        var builder = new StringBuilder();
        foreach (string st in _queue)
        {
            builder.Append(st).Append("\n");
        }

        _currentText = builder.ToString();
    }

    private void OnGUI()
    {
        if (Debug.isDebugBuild)
        {
            GUI.Label(
               new Rect(
                   5,                   // x, left offset
                   0, // y, bottom offset
                   500f,                // width
                   200f                 // height
               ),
               _currentText,             // the display text
               GUI.skin.textArea        // use a multi-line text area
            );
        }
    }
}
