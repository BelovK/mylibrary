﻿using UnityEngine;
using System.Collections;

namespace Lib {

	/// <summary>
	/// Adding comments to GameObjects in the Inspector.
	/// </summary>
	public class Comments : MonoBehaviour {
	
		/// <summary>
		/// The comment.
		/// </summary>
		[Multiline]
		public string text = "Click here and write your comment...";
	}
}
