using UnityEngine;

namespace Lib
{
    public class CameraLookObject : MonoBehaviour
    {
        [Header("All fields may be null")]
        [SerializeField] protected Camera _camera;
        [SerializeField] protected Renderer _rendererVisible;
        [SerializeField] protected Transform _projectTransform;

        protected Vector3 _directionToCamera;

        public void SetCamera(Camera camera)
        {
            _camera = camera;
        }

        private void Awake()
        {
            if (!_camera)
            {
                _camera = Camera.main;
            }
        }

        private void Update()
        {
            if (!_camera)
            {
                return;
            }
            if (_rendererVisible)
            {
                if (!_rendererVisible.isVisible)
                {
                    return;
                }
            }
            _directionToCamera = transform.position - _camera.transform.position;
            if (_projectTransform)
            {
                _directionToCamera = Vector3.ProjectOnPlane(_directionToCamera, _projectTransform.forward);
            }
            transform.rotation = Quaternion.LookRotation(_directionToCamera);
        }
    }
}