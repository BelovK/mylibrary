using System;
using UnityEngine;

namespace Lib
{
    public class TabUI : MonoBehaviour
    {
        public int TabCount => _tabs.Length;
        public int CurrentTabIndex { get; private set; } = 0;

        public event Action<int> OnTabClicked;
        public event Action OnTabInited;

        [SerializeField] private GameObject[] _tabs;

        private void Start()
        {
            CheckEdges();
            foreach (GameObject tab in _tabs)
            {
                tab.SetActive(false);
            }
            SetTab(CurrentTabIndex);
            OnTabInited?.Invoke();
        }

        public virtual void SetTab(int index)
        {
            DisactivateCurrentTab();
            CurrentTabIndex = index;
            ActivateCurrentTab();
            OnTabClicked?.Invoke(CurrentTabIndex);
        }

        public void NextTab()
        {
            DisactivateCurrentTab();
            CurrentTabIndex++;
            if (IsIndexEqualsMaxTab())
            {
                ClampMaxTabIndex();
            }
            else
            {
                SetTab(CurrentTabIndex);
            }
        }

        public void PreviousTab()
        {
            DisactivateCurrentTab();
            CurrentTabIndex--;
            if (IsIndexEqualsMinTab())
            {
                ClampMinTabIndex();
            }
            else
            {
                SetTab(CurrentTabIndex);
            }
        }

        private void DisactivateCurrentTab()
        {
            if (IsTabOutOfRange())
            {
                Debug.LogError("[TabUI] Index out of range");
                return;
            }
            _tabs[CurrentTabIndex].SetActive(false);
        }
        private void ActivateCurrentTab()
        {
            if (IsTabOutOfRange())
            {
                Debug.LogError("[TabUI] Index out of range");
                return;
            }
            _tabs[CurrentTabIndex].SetActive(true);
        }

        private void CheckEdges()
        {
            if (IsIndexEqualsMaxTab())
            {
                ClampMaxTabIndex();
            }
            if (IsIndexEqualsMinTab())
            {
                ClampMinTabIndex();
            }
        }

        protected virtual void ClampMaxTabIndex() => SetTab(TabCount - 1);
        protected virtual void ClampMinTabIndex() => SetTab(0);

        protected bool IsTabNotEdge() => !(IsIndexEqualsMaxTab() || IsIndexEqualsMinTab());
        protected bool IsTabOutOfRange() => CurrentTabIndex > GetMaxTabIndex() || CurrentTabIndex < 0;

        protected bool IsIndexEqualsMaxTab() => CurrentTabIndex >= GetMaxTabIndex();
        protected bool IsIndexEqualsMinTab() => CurrentTabIndex <= 0;

        private int GetMaxTabIndex() => TabCount - 1;
    }
}
