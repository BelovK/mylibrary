using System;
using UnityEngine;
using UnityEngine.UI;

namespace Lib
{
    public class EnumTabInputUI : TabUI
    {
        [SerializeField] private Button[] _buttons;

        public override void SetTab(int index)
        {
            foreach (Button button in _buttons)
            {
                button.interactable = true;
            }
            base.SetTab(index);
            _buttons[CurrentTabIndex].interactable = false;
        }

        private void OnEnable()
        {
            for (int i = 0; i < _buttons.Length; i++)
            {
                int index = i;
                Button button = _buttons[index];
                button.onClick.AddListener(() => SetTab(index));
            }
        }

        private void OnDisable()
        {
            for (int i = 0; i < _buttons.Length; i++)
            {
                int index = i;
                Button button = _buttons[index];
                button.onClick.RemoveListener(() => SetTab(index));
            }
        }


    }
}
