﻿using UnityEngine;
using UnityEngine.UI;

namespace Lib
{
    public class HorizontalTabInputUI : TabUI
    {
        [SerializeField] private Button _leftButton;
        [SerializeField] private Button _rightButton;


        private void OnEnable()
        {
            _leftButton.onClick.AddListener(PreviousTab);
            _rightButton.onClick.AddListener(NextTab);
        }

        private void OnDisable()
        {
            _leftButton.onClick.RemoveListener(PreviousTab);
            _rightButton.onClick.RemoveListener(NextTab);
        }

        protected override void ClampMaxTabIndex()
        {
            base.ClampMaxTabIndex();
            _rightButton.interactable = false;
        }

        protected override void ClampMinTabIndex()
        {
            base.ClampMinTabIndex();
            _leftButton.interactable = false;
        }

        public override void SetTab(int index)
        {
            base.SetTab(index);
            if (!IsIndexEqualsMinTab())
            {
                _leftButton.interactable = true;
            }
            if (!IsIndexEqualsMaxTab())
            {
                _rightButton.interactable = true;
            }
        }

    }
}
