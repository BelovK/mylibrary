﻿using Cysharp.Threading.Tasks;
using System;
using UnityEngine;

namespace Lib
{
    public class CameraFollowObject
    {
        public bool UseUpLookProject { get; set; } = false;

        protected readonly Camera _camera;
        protected readonly Transform _transform;

        private float _rotateSmoothMultiplayer = 1f;
        private Vector3 _offset = new Vector3(0, -0.2f, 0);

        private Func<bool> _isActive;

        public CameraFollowObject(Camera playerCamera, Transform objectTrasform)
        {
            _camera = playerCamera;
            _transform = objectTrasform;
            SetIsActiveCheck(() => _transform.gameObject.activeInHierarchy);
        }

        public CameraFollowObject(Camera playerCamera, Transform objectTrasform, float rotateSmoothMultiplayer = 1f)
            : this(playerCamera, objectTrasform)
        {
            _rotateSmoothMultiplayer = rotateSmoothMultiplayer;
        }

        public void SetIsActiveCheck(Func<bool> isActive) => _isActive = isActive;

        public void SetRotateSmoothMultiplayer(float rotateSmoothMultiplayer)
        {
            _rotateSmoothMultiplayer = rotateSmoothMultiplayer;
        }

        public void SetOffset(Vector3 offset) => _offset = offset;

        public void UpdatePosition()
        {
            if (_camera && _isActive())
            {
                _transform.position = _camera.transform.position + _offset;
                _transform.rotation = GetRotation();
            }
        }

        public void ResetPosition()
        {
            _transform.position = _camera.transform.position + _offset;
            _transform.rotation = GetLookRotation();
        }

        protected virtual Quaternion GetRotation()
        {
            float lerpValue = Time.deltaTime * _rotateSmoothMultiplayer;
            Quaternion lookRoation = GetLookRotation();
            return Quaternion.Lerp(_transform.rotation, lookRoation, lerpValue);
        }

        protected Quaternion GetLookRotation()
        {
            Vector3 forward = UseUpLookProject ? Vector3.ProjectOnPlane(_camera.transform.forward, Vector3.up) : _camera.transform.forward;
            Quaternion lookRoation = Quaternion.LookRotation(forward, Vector3.up);
            return lookRoation;
        }


    }
}
