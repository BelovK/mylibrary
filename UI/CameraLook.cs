using UnityEngine;

namespace Lib
{
    public class CameraLook : MonoBehaviour
    {
        [SerializeField] protected GameObject _lookObject;
        [SerializeField] protected Vector3 _projectVector = Vector3.up;
        protected Camera _camera;

        private Vector3 _vectorProjectLook;

        public virtual void Hide()
        {
            if(_lookObject != null )
                _lookObject.SetActive(false);
        }

        public virtual void Show() => _lookObject?.SetActive(true);

        public virtual void UpdateRotation()
        {
            _vectorProjectLook = _camera.transform.position - transform.position;
            if (_vectorProjectLook != Vector3.zero)
                _vectorProjectLook = Vector3.ProjectOnPlane(_vectorProjectLook, _projectVector);
            if (_vectorProjectLook != Vector3.zero)
            {
                _lookObject.transform.rotation = Quaternion.LookRotation(_vectorProjectLook);
            }
        }

        public virtual bool CheckActive => _lookObject.gameObject.activeInHierarchy && _camera;

        protected void SetMainCamera()
        {
            _camera = Camera.main;
        }

        protected virtual void OnEnable()
        {
            SetMainCamera();
        }


        private void Update()
        {
            if (CheckActive)
            {
                UpdateRotation();
            }
        }
    }
}
