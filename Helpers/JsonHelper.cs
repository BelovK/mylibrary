using System;
using UnityEngine;

namespace Lib
{
	public class JsonHelper
	{
		public static T[] FromJson<T>(string json)
		{
			Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
			return wrapper.users;
		}

        public static string ToJson<T>(T[] array)
		{
			Wrapper<T> wrapper = new Wrapper<T>();
			wrapper.users = array;
			return JsonUtility.ToJson(wrapper);
		}

		[Serializable]
		private class Wrapper<T>
		{
			public T[] users;
		}
	}
}
