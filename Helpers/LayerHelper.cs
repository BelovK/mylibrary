using System.Threading.Tasks;
using UnityEngine;

namespace Lib
{
    public class LayerHelper : MonoBehaviour
    {

        public const string LAYER_DEFAULT = "Default";

        /// <summary>
        /// Use it if not required immediately change layers
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="newLayer"></param>
        /// <returns></returns>
        public static async Task SetLayerRecursively(GameObject obj, int newLayer)
        {
            if (null == obj)
            {
                return;
            }

            obj.layer = newLayer;

            foreach (Transform child in obj.transform)
            {
                if (null == child)
                {
                    continue;
                }
                await SetLayerRecursively(child.gameObject, newLayer);
            }
        }

        public static async Task SetLayerRecursivelyDefault(GameObject obj)
        {
            await SetLayerRecursively(obj, LayerMask.NameToLayer(LAYER_DEFAULT));
        }
    }
}
