using System.Text;
using UnityEditor;
using UnityEngine;

namespace Lib
{
    public class AssetHelper
    {
        private const string RESOURCES_NAME = "Resources";
        private const string PREFAB_EXTENSION = ".prefab";
#if UNITY_EDITOR
        public static string GetAssetPath(GameObject prefab)
        {
            if (!prefab)
                return null;
            string guid;
            long file;

            string path = null;

            if (AssetDatabase.TryGetGUIDAndLocalFileIdentifier(prefab, out guid, out file))
            {
                path = AssetDatabase.GUIDToAssetPath(guid);
            }

            return path;
        }

        public static string GetResourcesPath(GameObject prefab)
        {
            if (!prefab)
            {
                Debug.LogError("[ASSET HELPER] Prefab is null");
            }
            StringBuilder stringBuilder = new StringBuilder(GetAssetPath(prefab));
            if (stringBuilder.ToString().Contains(RESOURCES_NAME))
            {
                stringBuilder.Remove(0, stringBuilder.ToString().LastIndexOf(RESOURCES_NAME) + RESOURCES_NAME.Length + 1);
            }
            stringBuilder.Remove(stringBuilder.ToString().LastIndexOf(PREFAB_EXTENSION), PREFAB_EXTENSION.Length);
            return stringBuilder.ToString();
        }


#endif
    }
}
