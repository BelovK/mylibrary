namespace Lib
{
	public enum MathSign
	{
		Plus,
		Minus,
		Divide,
		Multiply
	}

	public static class MathSignHelper
	{
		public const string PLUS_SIGN = "+";
		public const string MINUS_SIGN = "-";
		public const string MULTIPLY_SIGN = "*";
		public const string DIVIDE_SIGN = "/";

		public static string ToString(MathSign sign)
		{
			switch (sign)
			{
				case MathSign.Plus:
					return PLUS_SIGN;
				case MathSign.Minus:
					return MINUS_SIGN;
				case MathSign.Divide:
					return DIVIDE_SIGN;
				case MathSign.Multiply:
					return MULTIPLY_SIGN;
				default:
					return "";
			}
		}

		public static MathSign ToMathSign(string textSign)
		{
			switch (textSign)
			{
				case PLUS_SIGN:
					return MathSign.Plus;
				case MINUS_SIGN:
					return MathSign.Minus;
				case DIVIDE_SIGN:
					return MathSign.Divide;
				case MULTIPLY_SIGN:
					return MathSign.Multiply;
				default:
					throw new System.Exception("Convert error. Incorrect value: " + textSign);
			}
		}
	}
}
